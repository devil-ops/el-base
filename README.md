OIT SSI Systems EL7 Base Images
===============================

This repository contains the OIT SSI Systems EL7 Base Image files.  The current production base image is built from the `el7` branch.

Branches
--------

* el7: Production base image from centos:centos7
* el7-staging: Staging branch

Runit Init System
-----------------

The `el7` image uses the [runit init system](http://smarden.org/runit/).  This image is designed to be the partent image for other container images, and enables `runsvdir -P /etc/service` as the default entrypoint.  There are no services included in this parent image.

_How to add a service in a child image_

The `runsvdir -P /etc/service` entrypoint will look in `/etc/service` for services to startup and manage. Child images of the `el7` base image can add new services by creating a directory in the `/etc/service` directory.  The directory should be named after the service, and contain an executible file named `run`.  The `run` file should startup the service.

For example, to enable a very basic Apache service, the following is added to the Dockerfile for the child image:

```
RUN mkdir -p /etc/service/httpd
ADD httpd.run /etc/service/httpd/run
RUN chmod -R a+x /etc/service/httpd
```

And the httpd `run` file contains:

```
#!/usr/bin/env bash

set -o errexit
set -o pipefail

if [ -f /etc/sysconfig/httpd ]; then
  . /etc/sysconfig/httpd
fi

exec /usr/sbin/httpd -DFOREGROUND
```

_Note:_ Bash (or other shell) scripts do not handle signals correctly by default.  To ensure your service behaves when the container is sent TERM, KILL, etc. signals, the `exec` command should always be used to start the actual service process.

SSMTP Config
------------

The `el7` base image includes the `SSMTP` package.  `SSMTP` is a daemon-less email delivery program, and is configured by default to send outgoing mail to `smtp.duke.edu` over TLS.

`SSMTP` replaces the `mailq`, `newaliases` and `sendmail` commands.

RPM Repositories
----------------

The `el7` base image replaces the regular CentOS mirror list to use `archive.linux.duke.edu`, mostly for performance.

In addition to the CentOS base image repositories, the `el7` image includes the CentOS EPEL repo (from archive.linux), and the OIT CI repo (from yum.oit - for the runit RPM).

Integrity Testing Suite
-----------------------

The `el7` base image includes a small python-based test suite for validating images after they are built, to try to catch the small number of images that may build incorrectly but still finish building without error.

Tests are specified in a yaml format, and when added to the `/tests.d` directory of the child image, can be run with the `/run-tests.py` script inside a container built from that image.

_The test yaml format spec_

YAML files in `/tests.d` are discovered and run alphabetically.  Files are first run through a YAML Lint test, and the test suite will fail if the YAML is not valid.

Any valid YAML file that includes a top-level `tests` object is considered a test. Any number of YAML files can be included.  Each YAML file can have multiple tests.

The `tests` object should be described as in the example below.

```
tests:
  - test_name:
      cmd: command_to_validate
      validate:
        exit: exit_code_to_validate

  # Valid validations are:
  # exit: some_exit_code
  # stdout: some_string
  # stderr: some_string
```

By running tests alphabetically, more complicated tests can be staged.  Generally the YAML files start with a number, eg: `11-test-httpd-init.yml`.  This allows lower-numbered files to be run first, potentially setting up depenencies for later tests, in this example, copying SSL certificates or adding test content to download.

Automating Tests with Pre-Commit Hooks
--------------------------------------

The `el7` base image test suite can be used to validate image builds prior to commiting code to your git repository.  Using `docker-compose` and `git` pre-commit hooks, changes to child images can be tested and rejected on your local development machine before the commit occurs.

```
# docker-compose-test.yml

version: '2'
services:
  test:
    build:
      context: .
    image: stevedore-repo.oit.duke.edu/el-base-el7:latest
```

```
# .git/hooks/pre-commit

#!/bin/sh

set -o errexit

# Validate test YAML
yamllint tests.d || exit 1

exec bash -c "
  docker-compose --file docker-compose-test.yml \
                 build test && \
  docker-compose --file docker-compose-test.yml \
                 run \
                 --rm \
                 --entrypoint=/run-tests.py test"
```




