#!/usr/bin/env python3

import os
import sys
import yaml
import subprocess
import collections

class FailedTest(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class BadTestYAMLData(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def load(infile):

  with open(infile, 'r') as input:
    try:
      return(yaml.load(input))
    except yaml.YAMLError as exc:
      return(exc)


def run_command(command):
    ReturnResult = collections.namedtuple("ReturnResult", ['returncode', 'stdout', 'stderr'])
    # Run the command, with shell since we don't know if users
    # will do output redirection or shell expansion
    process = subprocess.Popen(command, shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    out, err = process.communicate()
    returncode = process.returncode

    return_tuple = ReturnResult(returncode, out, err)
    return return_tuple


def validate(name, expected, result, result_obj):
    if expected == result:
        print("{0:>{1}}".format('[Passed]', 55 - len(name)), flush=True)
        return 0
    else:
        print("{0:>{1}}".format('[FAILED]', 55 - len(name)))
        print(">> expected: '{}'".format(expected))
        print(">> returned: '{}'".format(result))
        print(">> stdout:")
        print(result_obj.stdout)
        print(">> stderr:")
        print(result_obj.stderr, flush=True)
        raise FailedTest("{}:{}".format(name,result))


def run_tests(list):
    for test in list:
        for name, data in test.items():
            print("Testing: '{}'".format(name), end='', flush=True)

            if len(data['validate']) > 1:
                raise BadTestYAMLData("{} has too many validations - only one allowed".format(name))

            command=data["cmd"]
            result = run_command(command)

            if 'exit' in data['validate']:
                validate(name, data['validate']['exit'], result.returncode, result)
            elif "stdout" in data['validate']:
                validate(name, data['validate']['stdout'].rstrip(), result.stdout.decode('UTF-8').rstrip(), result)
            elif "stderr" in data['validate']:
                validate(name, data['validate']['stderr'].rstrip(), result.stderr.decode('UTF-8').rstrip, result)
            else:
                print("Unknown Validation")
                raise BadTestYAMLData(data['validate'])


def doit():
    test_dir = "/tests.d"
    fail = False

    print("Starting tests", flush=True)

    try:
      # Lint
      print("Linting YAML test files for syntax", end='', flush=True)
      result = run_command("yamllint {}".format(test_dir))
      validate("YAML Linter", 0, result.returncode, result)
    except BadTestYAMLData as e:
        print("{}: syntax error".format(file))
        print(e)
        fail = True
    except FailedTest as e:
        print('One or more tests have failed in this file.')
        print(e)
        fail = True

    for file in sorted(os.listdir(test_dir)):
        if file.endswith(".yml"):
            yml = load(os.path.join(test_dir, file))

            if 'tests' in yml:
                try:
                    print("\nFrom: {}".format(file), flush=True)
                    run_tests(yml['tests'])
                except BadTestYAMLData as e:
                    print("{}: syntax error".format(file))
                    print(e, flush=True)
                    fail = True
                except FailedTest as e:
                    print('One or more tests have failed in this file.')
                    print(e, flush=True)
                    fail = True

    if fail:
        print('One or more tests have failed.', flush=True)
        sys.exit(1)

if __name__ == '__main__':
  doit()
