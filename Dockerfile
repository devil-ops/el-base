FROM image-mirror-prod-registry.cloud.duke.edu/library/centos:7
LABEL maintainer Chris Collins <christopher.collins@duke.edu>

LABEL name 'el-base-el7'
LABEL version '3.0'
LABEL release '0'
LABEL summary 'OIT Systems Generic el7 container'
LABEL description 'EL-based base image for OIT SSI Systems containers'
LABEL vendor 'Duke University, Office of Information Technology, Automation'
LABEL authoritative-source-url 'https://git-internal.oit.duke.edu/oit-docker/el-base'
LABEL changelog-url 'https://git-internal.oit.duke.edu/oit-docker/el-base/activity'
LABEL architecture 'x86_64'
LABEL distribution-scope 'private'

LABEL RUN 'n/a'
LABEL INSTALL 'n/a'
LABEL UNINSTALL 'n/a'

ENTRYPOINT ["/sbin/runsvdir", "-P", "/etc/service"]

# Runit: init system
# SSMTP: simple mail delivery
# TAR: support for Source2Image
# Python, python-yaml, yamllint, daemonize: Test suite dependencies
ENV TERM='xterm' \
    INSTALLED_PACKAGES='runit deltarpm ssmtp tar python python-yaml yamllint daemonize'

# Use local repos
ADD RPM-GPG-KEY-EPEL-7 /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
ADD oit-ci.repo /etc/yum.repos.d/oit-ci.repo

RUN sed -i 's/^#baseurl/baseurl/' /etc/yum.repos.d/* \
      && sed -i 's/^mirrorlist/#mirrorlist/' /etc/yum.repos.d/* \
      && sed -i 's/mirror.centos.org/archive.linux.duke.edu/' /etc/yum.repos.d/*

ADD epel.repo /etc/yum.repos.d/epel.repo

RUN unlink /etc/localtime \
      && ln -s /usr/share/zoneinfo/US/Eastern /etc/localtime

RUN yum update --setopt=tsflags=nodocs -y \
      && yum install --setopt=tsflags=nodocs -y ${INSTALLED_PACKAGES} \
      && yum clean all \
      && rm -rf /var/cache/yum

# SSMTP non-daemon mail sender
ADD ssmtp.conf /etc/ssmtp/ssmtp.conf
RUN chown root.mail /etc/ssmtp/ssmtp.conf \
      && chmod 640 /etc/ssmtp/ssmtp.conf

# CentOS base removes /boot but we test with 'rpm -V filesystem'  This puts back the
# expected /boot
# We tried expecting no /boot, but some packages installed in child images added it
# back with the wrong permissions which broke tests.
RUN mkdir -p -m 555 /boot

# Add test suite
ADD src/ /src/
ADD tests.d/ /tests.d/
ADD run-tests.py /run-tests.py
