pipeline {
  agent {
    node {
      label 'atomic'
    }
  }
  triggers {
    cron '@weekly'
  }
  stages {
    stage ('Build Image') {
      steps {
        sh '''
             source /build/pipeline_vars
             build
           '''
        }
    }
    stage('Test') {
      steps {
        parallel (
          Integrity: {
            sh '''
               source /build/pipeline_vars
               test
               '''
          },
          Unit: {
            echo "System image - no unit tests.  [PASS]"
          },
          Integration: {
            echo "Integration Tests Here"
          },
          SysSecurity: {
            sh '''
              source /build/pipeline_vars
              check_cves 7.0
            '''
          }
        )
      }
    }
    stage('Deploy') {
      steps {
        sh '''
           source /build/pipeline_vars
           tag
           '''
      }
    }
  }
  post { 
      always { 
          sh '''
             source /build/pipeline_vars
             cleanup_images
             '''
      }
      failure {
        mail(from: "doozer@build.docker.oit.duke.edu",
             to: "christopher.collins@duke.edu",
             subject: "${JOB_NAME} Image Build Failed",
             body: "Something failed with the ${JOB_NAME} image build: ${RUN_DISPLAY_URL}")
      }
  }
}
